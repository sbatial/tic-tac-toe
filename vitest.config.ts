import { defineConfig } from 'vitest/config';
import { svelte } from '@sveltejs/vite-plugin-svelte';

export default defineConfig({
	test: {
		includeSource: ['src/**/*.{js,ts}']
	},
	plugins: [svelte()]
});
