import { writable } from 'svelte/store';
import { toastStore } from '@skeletonlabs/skeleton';
import { negativeResponses, playfulNegativeResponses } from './responses';

const { update, subscribe, set } = writable<{
	currentPlayer: CellState;
	board: BoardState;
	won: CellState;
}>({
	currentPlayer: 'none',
	board: 'empty',
	won: 'none'
});

const isGameOver = (board: BoardState): boolean =>
	board != 'empty' &&
	// --- columns --- //
	((board[0] != 'none' && board[0] === board[3] && board[3] === board[6]) ||
		(board[1] != 'none' && board[1] === board[4] && board[4] === board[7]) ||
		(board[2] != 'none' && board[2] === board[5] && board[5] === board[8]) ||
		// --- rows --- //
		(board[0] != 'none' && board[0] === board[1] && board[1] === board[2]) ||
		(board[3] != 'none' && board[3] === board[4] && board[4] === board[5]) ||
		(board[6] != 'none' && board[6] === board[7] && board[7] === board[8]) ||
		// --- diagonals --- //
		(board[0] != 'none' && board[0] === board[4] && board[4] === board[8]) ||
		(board[2] != 'none' && board[2] === board[4] && board[4] === board[6]));

// NOTE: tested inline because `isGameOver` is currently not public
if (import.meta.vitest) {
	const { it, expect, describe, expectTypeOf } = import.meta.vitest;

	describe('game-over', () => {
		it('type check', () => {
			expectTypeOf(isGameOver).toBeFunction();
		});

		it('failing', () => {
			expect(isGameOver('empty')).toBe(false);
			expect(
				// prettier-ignore
				isGameOver(['none', 'none', 'none',
					        'none', 'none', 'none',
					        'none', 'none', 'none' ])
			).toBe(false);
		});
		it('columns', () => {
			// --- columns --- //
			expect(
				// prettier-ignore
				isGameOver(['o', 'none', 'none',
                            'o', 'none', 'x',
                            'o', 'none', 'x'])
			).toBe(true);
			expect(
				// prettier-ignore
				isGameOver(['none', 'x', 'none',
                            'o',    'x', 'none',
                            'o',    'x', 'none'])
			).toBe(true);
			expect(
				// prettier-ignore
				isGameOver(['none', 'o',    'x',
                            'none', 'none', 'x',
                            'o',    'none', 'x'])
			).toBe(true);
		});
		it('rows', () => {
			// --- rows --- //
			expect(
				// prettier-ignore
				isGameOver(['x',    'x',    'x',
                            'none', 'o',    'none',
                            'o',    'none', 'x'])
			).toBe(true);
			expect(
				// prettier-ignore
				isGameOver(['none', 'o', 'none',
                            'x',    'x', 'x',
                            'o',    'x', 'none'])
			).toBe(true);
			expect(
				// prettier-ignore
				isGameOver(['o', 'none', 'none',
                            'o', 'none', 'x',
                            'x', 'x',    'x'])
			).toBe(true);
		});
		it('diagonals', () => {
			// --- diagonals --- //
			expect(
				// prettier-ignore
				isGameOver(['x',    'none', 'none',
                            'o',    'x',    'o',
                            'none', 'none', 'x'])
			).toBe(true);
			expect(
				// prettier-ignore
				isGameOver(['none', 'x',    'o',
                            'none', 'o',    'x',
                            'o',    'none', 'x'])
			).toBe(true);
		});
	});
}

const takeTurn = (idx: number) =>
	update((state) => {
		const { currentPlayer, board, won } = state;

		if (board === 'empty') return state; // reject empty board without modifying it
		if (board[idx] != 'none') {
			toastStore.trigger({
				message: [...playfulNegativeResponses, ...negativeResponses][
					Math.round(Math.random() * (negativeResponses.length + playfulNegativeResponses.length))
				]
			});
			return state;
		} // do not modify already set cells
		if (isGameOver(board)) return state; // do not touch game state when someone has won

		board[idx] = currentPlayer;
		if (isGameOver(board)) return { ...state, won: currentPlayer }; // do not progress player when the current turn won the game

		return { currentPlayer: currentPlayer === 'x' ? 'o' : 'x', board, won };
	});

const setPlayer = (newPlayer: CellState) =>
	update((state) => ({ ...state, currentPlayer: newPlayer }));

const reset = () =>
	update(() => ({
		currentPlayer: Math.floor(Math.random() * 2) ? 'x' : 'o',
		board: ['none', 'none', 'none', 'none', 'none', 'none', 'none', 'none', 'none'],
		won: 'none'
	}));
reset(); // fill board initially

export const gameState = { subscribe, set, update, takeTurn, reset, setPlayer };
