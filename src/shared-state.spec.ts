import { describe, expect, expectTypeOf, it, vi } from 'vitest';
import { gameState } from './shared-state';

gameState.update = vi.fn();
vi.spyOn(gameState, 'update');
vi.spyOn(gameState, 'takeTurn').mockImplementation(() => {
	gameState.update(vi.mocked);
});

describe('take turn', () => {
	it('type check', () => {
		expectTypeOf(gameState.takeTurn).toBeFunction();
	});

	// TODO: Implement store tests here and component tests where the functions are used
	it.todo('winning turn');

	it.todo('empty board');
	it('no player is set', () => {
		gameState.takeTurn(0);
		console.log(gameState.takeTurn);
		expect(gameState.update).toHaveBeenCalled();
	});
	it.todo("turn with 'won' set");
});

describe('set player', () => {
	it('type check', () => {
		expectTypeOf(gameState.setPlayer).toBeFunction();
	});

	it.todo('implementation');
});

describe('reset board', () => {
	expectTypeOf(gameState.reset).toBeFunction();

	it.todo('implementation');
});
