// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	// interface Locals {}
	// interface PageData {}
	// interface Error {}
	// interface Platform {}
}

const states = ['x', 'o', 'none'] as const;
type CellState = (typeof states)[number];

type BoardState =
	| 'empty'
	| [
			CellState,
			CellState,
			CellState,
			CellState,
			CellState,
			CellState,
			CellState,
			CellState,
			CellState
	  ];
