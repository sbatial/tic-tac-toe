import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
	define: {
		'import.meta.vitest': 'undefined'
	},
	plugins: [sveltekit()]
});
